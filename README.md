# README for gtk3-classic-build-gtk3

## Overview
After using the [gtk-classic-build-rpm.sh](https://gitlab.com/bgstack15/gtk3-classic-build) script, a modified copy of Fedora gtk3 src.rpm exists on the local filesystem.

If this README exists in a directory named `for-repo`, then it is still in its source location from the above link, and not in its final location.

## This repository
This README file belongs in the modified copy of that Fedora gtk3 src.rpm [git repository](https://src.fedoraproject.org/rpms/gtk3). That modified git repo can be sent up to a [new web location](https://gitlab.com/bgstack15/gtk3-classic-build-gtk3).

Fedora [COPR](https://copr.fedorainfracloud.org/coprs/bgstack15/gtk3-classic/packages/) can then use rpkg to pull this new git repository, so users do not have to upload the 21MB srpm manually.

## Using this repository
The `gtk-classic-build-rpm.sh` script itself performs the work of bringing in this README file and fixes the .gitignore file.

## Differences from upstream repo
The [gtk3-classic](https://github.com/lah7/gtk3-classic) patchset makes some fundamental changes to gtk3, which are implemented in the srpm as a set of patches. The same .orig.tar.xz file is used.
Files that are added or modified:
* `*.patch` that are not named specifically here. The original gtk3 srpm had no .patch files.
* gtk3.spec
* this README.md
* .gitignore
